package net.minestom.server.terminal;

import net.minecrell.terminalconsole.SimpleTerminalConsole;
import net.minestom.server.MinecraftServer;

public class MinestomConsole extends SimpleTerminalConsole {
    @Override
    protected boolean isRunning() {
        return MinecraftServer.isStarted();
    }

    @Override
    protected void runCommand(String command) {

    }

    @Override
    protected void shutdown() {
        MinecraftServer.stopCleanly();
    }
}
